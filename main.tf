resource "aws_s3_bucket" "audit_trail" {
  bucket = var.s3_bucket_name
  count  = var.create_bucket ? 1 : 0
}

resource "aws_s3_bucket_server_side_encryption_configuration" "audit_trail" {
  count  = var.create_bucket ? 1 : 0
  bucket = aws_s3_bucket.audit_trail[0].id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = var.s3_encryption_type
    }
  }
}

resource "aws_s3_bucket_policy" "audit_trail" {
  count  = var.create_bucket ? 1 : 0
  bucket = aws_s3_bucket.audit_trail[0].id
  policy = data.aws_iam_policy_document.audit_trail.json
}

data "aws_iam_policy_document" "audit_trail" {
  statement {
    sid       = "AWSCloudTrailAclCheck"
    actions   = ["s3:GetBucketAcl"]
    effect    = "Allow"
    resources = ["arn:aws:s3:::${var.s3_bucket_name}"]

    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "Service"
    }
  }

  statement {
    sid       = "AWSCloudTrailWrite"
    actions   = ["s3:PutObject"]
    effect    = "Allow"
    resources = ["arn:aws:s3:::${var.s3_bucket_name}/*"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "Service"
    }
  }

  statement {
    sid       = "DenyIncorrectEncryptionHeader"
    actions   = ["s3:PutObject"]
    effect    = "Deny"
    resources = ["arn:aws:s3:::${var.s3_bucket_name}/*"]

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"
      values   = ["AES256"]
    }

    principals {
      identifiers = ["*"]
      type        = "*"
    }
  }

  statement {
    sid       = "DenyUnEncryptedObjectUploads"
    actions   = ["s3:PutObject"]
    effect    = "Deny"
    resources = ["arn:aws:s3:::${var.s3_bucket_name}/*"]

    condition {
      test     = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values   = ["true"]
    }

    principals {
      identifiers = ["*"]
      type        = "*"
    }
  }
}

resource "aws_cloudtrail" "audit" {
  name                          = var.name
  enable_logging                = var.enable_logging
  enable_log_file_validation    = var.enable_log_file_validation
  kms_key_id                    = var.kms_key_id
  s3_bucket_name                = var.create_bucket ? element(concat(aws_s3_bucket.audit_trail.*.id, [""]), 0) : var.s3_bucket_name
  s3_key_prefix                 = var.s3_key_prefix
  sns_topic_name                = var.sns_topic_name
  include_global_service_events = var.include_global_service_events
  is_multi_region_trail         = var.is_multi_region_trail
  is_organization_trail         = var.is_organization_trail
  cloud_watch_logs_group_arn    = var.cloud_watch_logs_group_arn
  cloud_watch_logs_role_arn     = var.cloud_watch_logs_role_arn

  depends_on = [
    aws_s3_bucket_server_side_encryption_configuration.audit_trail,
    aws_s3_bucket_policy.audit_trail,
  ]
}
