variable "name" {
  description = "The name of the trail you wish to create"
  type        = string
  default     = "audit-trail"
}

variable "enable_logging" {
  description = "Bool indicating whether to enable logging"
  type        = string
  default     = true
}

variable "kms_key_id" {
  description = "Specifies the KMS key ARN to use to encrypt the logs"
  type        = string
  default     = ""
}

variable "create_bucket" {
  description = "Bool indicating whether to create an S3 bucket for trail storage"
  type        = string
  default     = true
}

variable "s3_bucket_name" {
  description = "The name of the S3 bucket you wish to create for trail storage"
  type        = string
  default     = "cloudtrail-audit"
}

variable "s3_key_prefix" {
  description = "The key prefix to use when creating trail storage files"
  type        = string
  default     = "ct-audit"
}

variable "include_global_service_events" {
  description = "Bool indicating whether the trail is publishing events from global services such as IAM to the log files"
  type        = string
  default     = true
}

variable "sns_topic_name" {
  description = "Specifies the name of the Amazon SNS topic defined for notification of log file delivery"
  type        = string
  default     = ""
}

variable "is_multi_region_trail" {
  description = "Specifies whether the trail is created in the current region or in all regions"
  type        = string
  default     = true
}

variable "is_organization_trail" {
  description = "Specifies whether the trail is an AWS Organizations trail. Organization trails log events for the master account and all member accounts. Can only be created in the organization master account"
  type        = string
  default     = false
}

variable "cloud_watch_logs_group_arn" {
  description = "Specify a log group name ARN that represents the log group to which CloudTrail logs will be delivered"
  type        = string
  default     = ""
}

variable "cloud_watch_logs_role_arn" {
  description = "Specifies the role for the CloudWatch Logs endpoint to assume to write to a user's log group"
  type        = string
  default     = ""
}

variable "s3_encryption_type" {
  description = "type of encryption to use on S3 bucket AES or aws:kms"
  default     = "aws:kms"
}

variable "enable_log_file_validation" {
  description = "Specifies whether log file integrity validation is enabled"
  default     = false
}
