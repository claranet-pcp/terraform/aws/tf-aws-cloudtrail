output "cloudtrail_id" {
  value       = aws_cloudtrail.audit.id
  description = "ID of the created trail"
}

output "cloudtrail_s3_id" {
  value       = element(concat(aws_s3_bucket.audit_trail.*.id, [""]), 0)
  description = "ID of the created S3 bucket"
}

output "cloudtrail_s3_arn" {
  value       = element(concat(aws_s3_bucket.audit_trail.*.arn, [""]), 0)
  description = "ARN of the created S3 bucket"
}
