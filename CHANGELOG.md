## 2.0.0 (March 01, 2022)

IMPROVEMENTS:
* Terraform AWS provider 4.x support

NOTES:
* Requires Terraform >= 0.13.0
* Requires Terraform AWS Provider >= 4.0.0

## 0.1.0 (September 02, 2017)

IMPROVEMENTS:
* Added functionality to allow Cloudtrail to deliver logs to CloudWatch


## 0.0.5 (August 24, 2017)

BUG FIXES:
* Replaced S3 bucket name with bucket ID reference


## 0.0.4 (July 24, 2017)

BUG FIXES:
* Reverted change to S3 bucket creation, as Terraform wasn't happy about creating the bucket


## 0.0.3 (July 24, 2017)

IMPROVEMENTS:
* Terraform HCL formatted
* Now creates multi-region trails by default
* Now optionally creates S3 bucket, or re-uses existing bucket by name


## 0.0.2 (July 20, 2017)

BUG FIXES:
* Restricted S3 bucket policy to Cloudtrail service only


## 0.0.1 (July 04, 2016)

Initial version
