# tf-aws-cloudtrail

CloudTrail - Terraform Module

## Terraform version compatibility

| Module version    | Terraform version | AWS provider version |
|-------------------|-------------------|----------------------|
| 2.x.x             | >= 0.13.0         | >= 4.0.0             |
| 1.x.x             | 0.12.x            | -                    |
| 0.x.x             | 0.11.x            | -                    |

Upgrading from 0.11.x and earlier to 0.12.x should be seamless.  You can simply update the `ref` in your `source` to point to a version greater than `1.0.0`.

## Usage

```hcl
module "cloudtrail" {
  source                        = "git::https://gitlab.com/claranet-pcp/terraform/aws/tf-aws-cloudtrail.git?ref=v1.0.1"
  name                          = "my-cloudtrail"
  s3_bucket_name                = "my-cloudtrail-bucket"
  s3_key_prefix                 = ""
  enable_logging                = true
  include_global_service_events = true
  is_multi_region_trail         = false
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudtrail.audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudtrail) | resource |
| [aws_s3_bucket.audit_trail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.audit_trail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.audit_trail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_iam_policy_document.audit_trail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloud_watch_logs_group_arn"></a> [cloud\_watch\_logs\_group\_arn](#input\_cloud\_watch\_logs\_group\_arn) | Specify a log group name ARN that represents the log group to which CloudTrail logs will be delivered | `string` | `""` | no |
| <a name="input_cloud_watch_logs_role_arn"></a> [cloud\_watch\_logs\_role\_arn](#input\_cloud\_watch\_logs\_role\_arn) | Specifies the role for the CloudWatch Logs endpoint to assume to write to a user's log group | `string` | `""` | no |
| <a name="input_create_bucket"></a> [create\_bucket](#input\_create\_bucket) | Bool indicating whether to create an S3 bucket for trail storage | `string` | `true` | no |
| <a name="input_enable_log_file_validation"></a> [enable\_log\_file\_validation](#input\_enable\_log\_file\_validation) | Specifies whether log file integrity validation is enabled | `bool` | `false` | no |
| <a name="input_enable_logging"></a> [enable\_logging](#input\_enable\_logging) | Bool indicating whether to enable logging | `string` | `true` | no |
| <a name="input_include_global_service_events"></a> [include\_global\_service\_events](#input\_include\_global\_service\_events) | Bool indicating whether the trail is publishing events from global services such as IAM to the log files | `string` | `true` | no |
| <a name="input_is_multi_region_trail"></a> [is\_multi\_region\_trail](#input\_is\_multi\_region\_trail) | Specifies whether the trail is created in the current region or in all regions | `string` | `true` | no |
| <a name="input_is_organization_trail"></a> [is\_organization\_trail](#input\_is\_organization\_trail) | Specifies whether the trail is an AWS Organizations trail. Organization trails log events for the master account and all member accounts. Can only be created in the organization master account | `string` | `false` | no |
| <a name="input_kms_key_id"></a> [kms\_key\_id](#input\_kms\_key\_id) | Specifies the KMS key ARN to use to encrypt the logs | `string` | `""` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the trail you wish to create | `string` | `"audit-trail"` | no |
| <a name="input_s3_bucket_name"></a> [s3\_bucket\_name](#input\_s3\_bucket\_name) | The name of the S3 bucket you wish to create for trail storage | `string` | `"cloudtrail-audit"` | no |
| <a name="input_s3_encryption_type"></a> [s3\_encryption\_type](#input\_s3\_encryption\_type) | type of encryption to use on S3 bucket AES or aws:kms | `string` | `"aws:kms"` | no |
| <a name="input_s3_key_prefix"></a> [s3\_key\_prefix](#input\_s3\_key\_prefix) | The key prefix to use when creating trail storage files | `string` | `"ct-audit"` | no |
| <a name="input_sns_topic_name"></a> [sns\_topic\_name](#input\_sns\_topic\_name) | Specifies the name of the Amazon SNS topic defined for notification of log file delivery | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudtrail_id"></a> [cloudtrail\_id](#output\_cloudtrail\_id) | ID of the created trail |
| <a name="output_cloudtrail_s3_arn"></a> [cloudtrail\_s3\_arn](#output\_cloudtrail\_s3\_arn) | ARN of the created S3 bucket |
| <a name="output_cloudtrail_s3_id"></a> [cloudtrail\_s3\_id](#output\_cloudtrail\_s3\_id) | ID of the created S3 bucket |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
